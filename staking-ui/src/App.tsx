import React from 'react';
import Web3 from 'web3';
import BN from 'bn.js';
import BigNumber from "bignumber.js"

import { Maybe, Just, Nothing } from 'purify-ts/Maybe';
import { MaybeAsync } from 'purify-ts/MaybeAsync';
import { Switch, Route } from 'react-router-dom';

import HomePage from './pages/HomePage';
import StakePage from './pages/StakePage';
import UserStakesPage from './pages/UserStakesPage';
import Navigation from './components/Navigation';

import { getFirstAccount, getWeb3, getEthBalance } from './utils/Web3';
import { UserInfo, Contracts } from './utils/Types';
import { StakingToken } from './utils/contracts/StakingToken';
import { Stake, StakingV3 } from './utils/contracts/StakingV3';

import addresses from './utils/addresses/addresses-rinkeby.json';
import './styles/style.css';

let Contract = require('web3-eth-contract');

declare global {
  interface Window {
    ethereum : any;
  }
}


interface AppProp {
  web3: Maybe<Web3>,
  contracts: Maybe<Contracts>,
  userInfo: Maybe<UserInfo>
  intervalId: number,
}


class App extends React.Component<{}, AppProp> {
  constructor(props: AppProp) {
    super(props);
    this.state = {
      web3: Nothing,
      contracts: Nothing,
      userInfo: Nothing,
      intervalId: 0
    };
    this.setUserStakes = this.setUserStakes.bind(this);
    this.setTokenBalance = this.setTokenBalance.bind(this);
  }

  componentDidMount = async () => {
    const web3 = await getWeb3();
    await this.instantiateContracts(web3);
    this.setState({
      web3: web3,
      intervalId: window.setInterval(this.fetchUserState.bind(this), 1000)
    })
  }

  instantiateContracts = async (web3: Maybe<Web3>) => {
    web3.ifJust(w => {
      try {
        Contract.setProvider(w.currentProvider);
        var tokenContract = new StakingToken(addresses.StakingToken);
        var stakingContract = new StakingV3(addresses.StakingV1);

        this.setState({
          contracts: Just({
            tokenContract: tokenContract,
            stakingContract: stakingContract
          })
        })
      }
      catch (error) {
        console.log(error);
      }
    })
  }

  fetchUserState = async () => {
    this.state.web3.ifJust(async web3 => {
      const account = await getFirstAccount(web3);
      const userInfo = await this.fetchUserInfo(web3, account);
      this.setState({
        userInfo: userInfo
      })
    });
  }

  fetchUserInfo = async (web3: Web3, newAccount: Maybe<string>) =>  {
    const userInfo = MaybeAsync.liftMaybe(newAccount).chain(async a => {
      if(a !== this.state.userInfo.mapOrDefault(ui => ui.account, "undefined")) {
        const ethBalance = await getEthBalance(a, web3);
        const tokenBalance = await this.state.contracts.mapOrDefault(contracts => contracts.tokenContract.balanceOf(a), Promise.resolve(new BigNumber(0)))
        const userStakes = await this.state.contracts.mapOrDefault(contracts => contracts.stakingContract.getStakes(a), Promise.resolve([]));
        const data = Just({
            account: a,
            ethBalance: ethBalance,
            tokenBalance: tokenBalance,
            stakes: userStakes
        });
        return data;
      }
      return this.state.userInfo;
    });
    return userInfo
  }

  componentWillUnmount = () => {
    window.clearInterval(this.state.intervalId);
    this.setState = (state, callback) => {
      return;
    };
  }

  setUserStakes(stakes: Stake[]) {
    const newUserInfo: Maybe<UserInfo> = this.state.userInfo.map(userInfo => {
      return {
        account: userInfo.account,
        ethBalance: userInfo.ethBalance,
        tokenBalance: userInfo.tokenBalance,
        stakes: stakes
      }
    })
    this.setState({
      userInfo: newUserInfo
    })
  }

  setTokenBalance(tokenBalance: BigNumber) {
    const newUserInfo: Maybe<UserInfo> = this.state.userInfo.map(userInfo => {
      return {
        account: userInfo.account,
        ethBalance: userInfo.ethBalance,
        tokenBalance: tokenBalance,
        stakes: userInfo.stakes
      }
    })
    this.setState({
      userInfo: newUserInfo
    })
  }

  render() {
    const account = this.state.userInfo.mapOrDefault(ui => ui.account, "undefined");
    return (
      <div className="App">
        <Navigation account = { account }/>
        <Switch>
            <Route path="/" exact = {true}>
              <HomePage contracts = { this.state.contracts}
                        userInfo = { this.state.userInfo }
              />
            </Route>
            <Route path="/stake">
              <StakePage
                contracts = { this.state.contracts }
                userInfo = { this.state.userInfo }
                stakesHandler = { this.setUserStakes }
                tokenBalanceHandler = { this.setTokenBalance }
              />
            </Route>
            <Route path="/my-stakes">
              <UserStakesPage
                key={ this.state.contracts.isNothing() ? 1 : 0 }
                contracts = { this.state.contracts }
                userInfo = { this.state.userInfo }
                stakesHandler = { this.setUserStakes }
              />
            </Route>
          </Switch>
      </div>
    );
  }
}

export default App;