import { Maybe, Nothing } from 'purify-ts/Maybe';
import React, { ChangeEvent } from 'react';
import { Contracts, UserInfo } from '../utils/Types';
import StakeInfo from '../components/StakeInfo';
import { Stake } from './../utils/contracts/StakingV3';
import BN from 'bn.js';
import BigNumber from 'bignumber.js';

import './../styles/style.css';


interface UserStakesPageProp {
  contracts: Maybe<Contracts>,
  userInfo: Maybe<UserInfo>,
  stakesHandler: (stakes: Stake[]) => void
}

interface UserStakesPageState {
  contracts: Maybe<Contracts>,
  userInfo: Maybe<UserInfo>,
  rewardsDate: string,
  rewardsDateError: string,
  estimatedRewards: BigNumber[]
}


class UserStakesPage extends React.Component<UserStakesPageProp, UserStakesPageState> {
  constructor(props: UserStakesPageProp) {
    super(props);
    this.state = {
      contracts: Nothing,
      userInfo: Nothing,
      rewardsDate: "1",
        rewardsDateError: "",
        estimatedRewards: []
      }
    }

  componentWillReceiveProps(props:UserStakesPageProp) {
    this.setState({
      contracts: props.contracts,
      userInfo: props.userInfo
    })
  }

  fetchUserStakes = () => {
    this.state.contracts.ifJust(contracts => {
      this.state.userInfo.ifJust(async userInfo => {
        const stakes = await contracts.stakingContract.getStakes(userInfo.account);
        this.props.stakesHandler(stakes)
      })
    })
  }

  isRewardsDateValid = (rewardsDate: string) => {
    let timestamp = parseInt(rewardsDate)
    return !isNaN(timestamp) && timestamp >= 0 && timestamp <= 48
  }

  handleInputChange = (event: ChangeEvent<HTMLInputElement>): void => {
    this.setState({
      rewardsDate: event.target.value
    })
    if(this.isRewardsDateValid(event.target.value)) {
      this.setState({ rewardsDateError: "" })
      this.estimateRewards(event.target.value)
    }
    else {
      this.setState({
        estimatedRewards: [],
        rewardsDateError: "Rewards date must be an integer between 0-48."
      })
    }
  }

  estimateRewards = (diff: string) => {
    this.state.contracts.ifJust(contracts => {
      this.state.userInfo.ifJust(userInfo => {
        this.setState({ estimatedRewards: [] })
          const promises = userInfo.stakes.map(stake => contracts.stakingContract.applyInterest(stake.amount, new BigNumber(parseInt(diff))))
          Promise.all(promises)
            .then(rewards => this.setState({ estimatedRewards: rewards }))
      })
    })
  }

  componentWillUnmount = () => {
    this.setState = (state, callback) => { return; };
  }

  render() {
    const displayedStakes =
      this.state.userInfo.mapOrDefault(userInfo =>
        userInfo.stakes.map((stake, stakeIdx) => {
          const rewards = this.state.estimatedRewards[stakeIdx] === undefined ? stake.amount.toString() : this.state.estimatedRewards[stakeIdx];
          return <StakeInfo
            key={stakeIdx}
            contracts = { this.state.contracts }
            userInfo = { this.state.userInfo }
            stake = { stake }
            stakeIdx = { stakeIdx }
            estimatedRewards = { parseInt(rewards.toString()) }
          />
        }), []
      );
    return (
      <>
        <div className="stake-info">
          <p>Estimate rewards after</p>
          <form>
            <input type="number" value={this.state.rewardsDate} onChange={this.handleInputChange}/>
          </form>
          <br/>
          <p>months</p>
          <p>{ this.state.rewardsDateError }</p>
        </div>
        <div> { displayedStakes } </div>
      </>
    );
  }
}


export default UserStakesPage;