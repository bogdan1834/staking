import { Maybe, Nothing } from 'purify-ts/Maybe';
import React from 'react';
import { Contracts, UserInfo } from '../utils/Types';
import { formatEthBalance } from '../utils/Utils';

import './../styles/style.css';

interface HomePageProp {
  contracts: Maybe<Contracts>,
  userInfo: Maybe<UserInfo>,
}

interface HomePageState {
  contracts: Maybe<Contracts>,
  userInfo: Maybe<UserInfo>
}

class HomePage extends React.Component<HomePageProp, HomePageState> {
  constructor(props: HomePageProp) {
    super(props);
    this.state = {
      contracts: Nothing,
      userInfo: Nothing
    }
  }

  componentDidUpdate(prevProps: HomePageProp) {
    if(prevProps !== this.props) {
      this.setState({
        contracts: this.props.contracts,
        userInfo: this.props.userInfo
      });
    }
  }

  render() {
    const ethBalance = this.state.userInfo.mapOrDefault(ui => formatEthBalance(5, ui.ethBalance), "0")
    const erc20Balance = this.state.userInfo.mapOrDefault(ui => ui.tokenBalance.toString(), "0")
    return (
      <div id="main-div">
        <h1>Account details</h1>
        <p>Home page</p>
        <p>Eth balance: { ethBalance.toString() } ETH</p>
        <p>Token balance: { erc20Balance.toString() }</p>
        <h1>About our staking rewards</h1>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Magnam adipisci earum laborum corporis. Rem fugit illum inventore quas blanditiis? Tempore atque maxime quidem, ut a animi explicabo ducimus dicta. Iure.</p>
      </div>
    );
  }
}

export default HomePage;