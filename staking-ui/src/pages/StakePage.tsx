import { Maybe, Nothing } from 'purify-ts/Maybe';
import React, { ChangeEvent } from 'react';
import { Contracts, UserInfo } from '../utils/Types';
import BN from 'bn.js'
import BigNumber from 'bignumber.js'
import { CButton, CModal, CModalBody, CModalFooter, CModalHeader, CModalTitle } from '@coreui/react';
import './../styles/style.css';
import { estimateRewards, secondsPerYear } from '../utils/Utils';
import { Stake } from './../utils/contracts/StakingV3'

interface StakePageProp {
  contracts: Maybe<Contracts>,
  userInfo: Maybe<UserInfo>,
  stakesHandler: (stakes: Stake[]) => void
  tokenBalanceHandler: (balance: BigNumber) => void
}

interface StakePageState {
  contracts: Maybe<Contracts>,
  userInfo: Maybe<UserInfo>,
  stakeAmount: string,
  modalVisibility: boolean,
  stakeCountLimit: BigNumber,
  modalMessage: string,
  bodyMessage: string,
  hasAllowance: boolean
}


class StakePage extends React.Component<StakePageProp, StakePageState> {
  constructor(props: StakePageProp) {
    super(props);
    this.state = {
      contracts: Nothing,
      userInfo: Nothing,
      stakeAmount: "",
      modalVisibility: false,
      modalMessage: "",
      bodyMessage: "",
      hasAllowance: false,
      stakeCountLimit: new BigNumber(0)
    }
}

  componentDidUpdate = async (prevProps: StakePageProp) => {
    if(prevProps !== this.props) {
      this.setState({
        contracts: this.props.contracts,
        userInfo: this.props.userInfo,
        stakeCountLimit: await this.props.contracts.mapOrDefault(contracts =>
          contracts.stakingContract.stakeCountLimit(), Promise.resolve(new BigNumber(0)))
        });
      }
  }

  handleInputChange = (event: ChangeEvent<HTMLInputElement>): void => {
    this.setState({
      stakeAmount: event.target.value
    })
  }

  handleStake = (event: React.SyntheticEvent) => {
    event.preventDefault()
    this.state.userInfo.ifJust(userInfo => {
      if(userInfo.stakes.length >= parseInt(this.state.stakeCountLimit.toString())) {
        this.setState({
          bodyMessage: "You have reached your limit of 10 stakes. You must unstake at least one of them before staking a new amount."
        })
      }
      else {
        this.createStake()
      }
    })
  }

  resetmodalMessage = () => {
    this.setState({
      modalMessage: ""
    })
  }

  setStakeAllowance = (allowance: boolean) => {
    this.setState({
      hasAllowance: allowance
    })
  }

  setModalVisibility = (visibility: boolean) => {
    this.setState({
      modalVisibility: visibility
    })
  }

  createStake = () => {
    this.resetmodalMessage()
    this.state.contracts.ifJust(contracts => {
      this.state.userInfo.ifJust(async userInfo => {
        const allowance = await contracts.tokenContract.allowance(userInfo.account, contracts.stakingContract.web3Contract.options.address);
        const amount = new BigNumber(this.state.stakeAmount);
        console.log(typeof allowance);
        const hasAllowance = allowance.gte(amount);
        this.setStakeAllowance(hasAllowance)
        if(hasAllowance) {
          contracts.stakingContract
            .stake(new BigNumber(this.state.stakeAmount), { from: userInfo.account })
            .then(async _ => await this.updateUserStakes())
            .then(async _ => await this.updateTokenBalance())
        }
        this.setModalVisibility(!hasAllowance);
      })
    })
  }

  updateUserStakes = async () => {
    this.state.contracts.ifJust(contracts => {
      this.state.userInfo.ifJust(async userInfo => {
        const stakes = await contracts.stakingContract.getStakes(userInfo.account)
        this.props.stakesHandler(stakes)
      })
    })
  }

  updateTokenBalance = async () =>  {
    this.state.contracts.ifJust(contracts => {
      this.state.userInfo.ifJust(async userInfo => {
        const balance = await contracts.tokenContract.balanceOf(userInfo.account);
        this.props.tokenBalanceHandler(balance)
      })
    })
  }

  createAllowance = () => {
    this.state.contracts.ifJust(contracts => {
      this.state.userInfo.ifJust(async userInfo => {
        const receipt = await contracts.tokenContract.approve(contracts.stakingContract.web3Contract.options.address, new BigNumber(this.state.stakeAmount), { from: userInfo.account });
        this.setState({
          modalMessage: receipt.status ? "Allowance succeeded" : "Allowance failed",
          hasAllowance: receipt.status
        })
      })
    })
  }

  renderAllowanceBody() {
    return (
      <>
        <CModalBody>
          <p>Do you want to go ahead with a stake of { this.state.stakeAmount} tokens?</p>
          <CModalFooter>
            <CButton color="secondary" onClick={() => this.setModalVisibility(false)}>
              No
            </CButton>
            <CButton color="primary" onClick={this.handleStake}>
              Yes
            </CButton>
          </CModalFooter>
        </CModalBody>
      </>
    )
  }

  renderNoAllowanceBody() {
    return (
      <>
        { this.state.modalMessage }
          <CModalBody>
            <p>The staking contract requires you to set an allowance for the tokens you want to stake.</p>
            <p>Do you want to create an allowance of { this.state.stakeAmount}?</p>
          </CModalBody>
          <CModalFooter>
            <CButton color="secondary" onClick={() => this.setModalVisibility(false)}>
              No
            </CButton>
            <CButton color="primary" onClick={() => this.createAllowance()}>
              Yes
            </CButton>
          </CModalFooter>
      </>
    )
  }

  renderModal() {
    const { modalVisibility} = this.state;
    return (
      <>
      <CModal visible={ modalVisibility } onDismiss={() => this.setModalVisibility(false)}>
        <CModalHeader onDismiss={ () => this.setModalVisibility(false) }>
          <CModalTitle>Warning</CModalTitle>
        </CModalHeader>
        { !this.state.hasAllowance ?
          this.renderNoAllowanceBody() :
          this.renderAllowanceBody()
        }
        </CModal>
      </>
    )
  }

  render() {
    const estimatedRewards =
      this.state.stakeAmount !== "" ?
        estimateRewards({ amount: new BigNumber(this.state.stakeAmount), timestamp: new BigNumber(Date.now() / 1000) }, new Date(Date.now() + secondsPerYear())) :
        0
      const numOfStakes = this.state.userInfo.mapOrDefault(userInfo => userInfo.stakes.length, 0)
      return (
        <div id="main-div">
          <div id="stake-form">
          <form>
            <label>
              Stake amount<br/>
              <input type="number" value={this.state.stakeAmount} onChange={this.handleInputChange}/>
            </label>
            <br/>
            <button onClick={this.handleStake}>Stake</button>
            <p>{ estimatedRewards !== 0  && "Estimated rewards (1 year): ".concat(estimatedRewards.toString()) }</p>
            <p>{ numOfStakes } / { this.state.stakeCountLimit.toString() } stakes</p>
            <p>{ this.state.bodyMessage }</p>
            </form>
        </div>
        <br/>
        { this.renderModal() }
      </div>
    );
  }
}

export default StakePage;