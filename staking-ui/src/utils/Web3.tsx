import Web3 from 'web3';
import BN from 'bn.js';
import { Just, Nothing } from 'purify-ts/Maybe';
import { List } from 'purify-ts/List';

export const getWeb3 = async () => {
  if(window.ethereum) {
    return Just(new Web3(window.ethereum));
  }
  return Nothing;
}

export const getFirstAccount = async (web3: Web3) => {
  const accounts = await web3.eth.getAccounts(() => {});
  return List.head(accounts);
}

export const getEthBalance = async (address: string, web3: Web3) => {
  return web3.eth.getBalance(address);
}
