import { Stake } from './contracts/StakingV3'
import { fromWei } from 'web3-utils';
import BN from "bn.js";
import BigNumber from "bignumber.js";


export const estimateRewards = (stake: Stake, rewardsDate: Date) => {
  const interestIntervals = [
    0.15,
    0.125,
    0.1, 0.1, 0.1,
    0.09, 0.09, 0.09, 0.09, 0.09, 0.09,
    0.08, 0.08, 0.08, 0.08, 0.08, 0.08,
    0.07, 0.07, 0.07, 0.07, 0.07, 0.07, 0.07,
    0.06, 0.06, 0.06, 0.06, 0.06, 0.06,
    0.05
  ]

  const stakeDate = new Date(parseInt(stake.timestamp.toString()) * 1000);
  const elapsedMonths = dateDiffInMonths(stakeDate, rewardsDate);

  let amount = parseInt(stake.amount.toString())
  let coeff = 1

  const intervalCount = interestIntervals.length
  const iterCount = intervalCount < elapsedMonths ? intervalCount : elapsedMonths;

  for (let i = 0; i < iterCount; ++i)
    coeff *= (1 + interestIntervals[i] / 12)

  if (elapsedMonths > intervalCount)
    coeff *= (1 + interestIntervals[intervalCount - 1] / 12) ** (elapsedMonths - intervalCount)

  return amount * coeff
}


function dateDiffInMonths(a: Date, b: Date) {
  const _MS_PER_MONTH = 1000 * 60 * 60 * 24 * 30;

  const utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
  const utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());

  return Math.floor((utc2 - utc1) / _MS_PER_MONTH);
}

export const secondsPerYear = () => {
  return 1000 * 60 * 60 * 24 * 365;
}

export const formatEthBalance = (decimals: number, balance: BigNumber) => {
  const toEth = parseFloat(fromWei(balance.toString()))
  const withDecimals = toEth.toLocaleString("en-US", { maximumFractionDigits: decimals, minimumFractionDigits: 0 })
  return withDecimals
}
