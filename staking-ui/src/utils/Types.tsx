import BN from "bn.js";
import BigNumber from "bignumber.js";
import { StakingToken } from "./contracts/StakingToken";
import { Stake, StakingV3 } from "./contracts/StakingV3";


export interface UserInfo {
  account: string,
  ethBalance: BigNumber,
  tokenBalance: BigNumber,
  stakes: Stake[]
}

export interface Contracts {
  tokenContract: StakingToken,
  stakingContract: StakingV3
}
