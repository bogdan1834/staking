// This file was automatically generated by eth-binding-gen.
// It is NOT recommended to modify it manually, as any changes may be overwritten.

import { PromiEvent, TransactionReceipt } from "web3-core"
import { AbiItem } from "web3-utils"
import EventEmitter from "eventemitter3"
import { BigNumber } from "bignumber.js"
import { Contract } from "web3-eth-contract"
import { Address, Bytes, decodeInt, decodeArray, encodeArray } from "./Utils"
const _Contract = require("web3-eth-contract")

export class IERC165Upgradeable {
  static readonly jsonABI: AbiItem[] = [{"inputs":[{"internalType":"bytes4","name":"interfaceId","type":"bytes4"}],"name":"supportsInterface","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"view","type":"function"}]
  readonly web3Contract: Contract

  constructor(address: Address) {
    this.web3Contract = new _Contract(IERC165Upgradeable.jsonABI, address)
  }

  supportsInterface(interfaceId: Bytes): Promise<boolean> {
    return this.web3Contract.methods["0x01ffc9a7"](interfaceId).call()
  }
}
