import { BigNumber } from "bignumber.js"

export type Address = string

export type Bytes = string

export const decodeInt = (v: any) => {
  if (v instanceof BigNumber)
    return v
  else if (typeof v == "string" || typeof v == "number")
    return new BigNumber(v)
  else
    return new BigNumber(v.toString())
  }

export const decodeArray = <T>(d: (v: any) => T) => (v: any[]) => v.map(d)

export const encodeArray = <T>(e: (v: T) => any) => (v: T[]) => v.map(e)
