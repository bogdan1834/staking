import { CCollapse, CContainer, CNavbar, CNavbarBrand, CNavbarNav, CNavbarToggler, CNavItem } from '@coreui/react';
import "@coreui/coreui/dist/css/coreui.css";
import { Link } from 'react-router-dom';

import React from 'react';


interface NavigationProp {
  account: String
}

interface NavigationState {
  visible: boolean,
  account: String
}

class Navigation extends React.Component<NavigationProp, NavigationState> {
  constructor(props: NavigationProp) {
    super(props);
    this.state = {
      visible: false,
      account: "Invalid"
    }
  }

  setVisible(visibility: boolean) {
    this.setState({
      visible: visibility
    })
  }

  static getDerivedStateFromProps(nextProps: NavigationProp, prevState: NavigationState) {
    if(nextProps.account !== prevState.account) {
      return {
        visible : prevState.visible,
        account : nextProps.account
      };
    }
    return prevState;
  }

  render() {
    return (
      <>
        <CNavbar style={{backgroundColor: "#c28632", color: "#5e4017"}} expand="lg">
          <CContainer fluid>
            <CNavbarBrand href="#">Stake</CNavbarBrand>
            <CNavbarToggler onClick={() => this.setVisible(!this.state.visible)} />
            <CCollapse className="navbar-collapse" visible={this.state.visible}>
              <CNavbarNav>
                <CNavItem>
                  <Link className="nav-link" to="/"> Home </Link>
                </CNavItem>
                <CNavItem>
                  <Link className="nav-link" to="/stake"> Stake </Link>
                </CNavItem>
                <CNavItem>
                  <Link className="nav-link" to="/my-stakes"> My stakes </Link>
                </CNavItem>
                <CNavItem>
                  <Link className="nav-link" to="/"> Connected with: {this.state.account } </Link>
                </CNavItem>
              </CNavbarNav>
            </CCollapse>
          </CContainer>
        </CNavbar>
      </>
    )
  }
}

export default Navigation;