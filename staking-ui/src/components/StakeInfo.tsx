import { Maybe, Nothing } from 'purify-ts/Maybe';
import React from 'react';
import { Contracts, UserInfo } from '../utils/Types';
import { Stake } from './../utils/contracts/StakingV3';
import BN from 'bn.js'
import BigNumber from 'bignumber.js'

interface StakeProp {
  contracts: Maybe<Contracts>,
  userInfo: Maybe<UserInfo>,
  stake: Stake,
  stakeIdx: number,
  estimatedRewards: number
}

interface StakeState {
  contracts: Maybe<Contracts>,
  userInfo: Maybe<UserInfo>,
  stake: Stake,
  stakeIdx: number,
  estimatedRewards: number
}


class StakeInfo extends React.Component<StakeProp, StakeState> {
  constructor(props: StakeProp) {
    super(props);
    this.state = {
      contracts: Nothing,
      userInfo: Nothing,
      stake: {
        amount: new BigNumber(0),
        timestamp: new BigNumber(0)
      },
      stakeIdx: 0,
      estimatedRewards: 0
    }
  }

  componentDidUpdate = (prevProps: StakeProp) => {
    if(prevProps !== this.props) {
      this.setState({
        contracts: this.props.contracts,
        userInfo: this.props.userInfo,
        stake: this.props.stake,
        stakeIdx: this.props.stakeIdx,
        estimatedRewards: this.props.estimatedRewards
      });
    }
  }

  unstake = async (event: any) => {
    this.state.contracts.ifJust(contracts => {
      this.state.userInfo.ifJust(async userInfo => {
        await contracts.stakingContract.unstake(new BigNumber(this.state.stakeIdx), { from: userInfo.account });
      })
    })
  }

  render() {
    const amount = this.state.stake.amount.toString()
    const dateCreated = new Date(parseInt(this.state.stake.timestamp.toString()) * 1000).toUTCString();
    const estimatedRewards = this.state.estimatedRewards
    return (
      <div className="stake-info">
        <p>Stake amount: { amount }</p>
        <p>Date created: { dateCreated }</p>
        <p>Estimated rewards: { estimatedRewards } </p>
        <button onClick={ this.unstake }>Collect rewards</button>
      </div>
    );
  }
}


export default StakeInfo;