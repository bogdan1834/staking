// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/IERC20Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/introspection/IERC165Upgradeable.sol";
import "./IStakePool.sol";

contract StakePool is IStakePool, Initializable, OwnableUpgradeable {
  IERC20Upgradeable private tokenContract;

  function __StakePool_init(address _tokenContract) public initializer {
    __Context_init_unchained();
    __Ownable_init_unchained();
    __StakePool_init_unchained(_tokenContract);
  }

  function __StakePool_init_unchained(address _tokenContract) public initializer {
    IERC165Upgradeable erc165 = IERC165Upgradeable(_tokenContract);
    require(erc165.supportsInterface(type(IERC20Upgradeable).interfaceId), "Address must be an ERC20 contract.");

    tokenContract = IERC20Upgradeable(_tokenContract);
  }

  function initialize(address _tokenContract) public initializer {
    __StakePool_init(_tokenContract);
  }

  function refill(address _recipient, uint amount) public override onlyOwner {
    tokenContract.transferFrom(msg.sender, address(this), amount);
    tokenContract.approve(_recipient, tokenContract.balanceOf(address(this)));
  }

  function supportsInterface(bytes4 interfaceId) external override pure returns (bool) {
    return
      interfaceId == type(IERC165Upgradeable).interfaceId
      || interfaceId == type(IStakePool).interfaceId;
  }
}
