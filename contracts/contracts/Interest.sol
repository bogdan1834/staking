
// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "./UInt512.sol";

library Interest {
  using UInt512 for uint256;
  using UInt512 for uint512;
  using Interest for uint512;

  function applyInterestV1(uint512 memory amount, uint yearlyInterestPerMille, uint elapsedMonths) internal pure returns(uint512 memory) {
    uint512 memory _12000 = uint512(0, 12000);
    uint512 memory _yearlyInterestPerMille = uint512(0, yearlyInterestPerMille);
    uint512 memory _elapsedMonths = uint512(0, elapsedMonths);

    return amount.mul(_12000.add(_yearlyInterestPerMille.mul(_elapsedMonths))).div(_12000);
  }

  function applyInterestV2(uint512 memory amount, uint yearlyInterestPerMille, uint elapsedMonths) internal pure returns(uint512 memory) {
    return amount.mul((12000 + yearlyInterestPerMille).pow(elapsedMonths)).div(uint(12000).pow(elapsedMonths));
  }

  function min(uint a, uint b) private pure returns(uint) {
    return a < b ? a : b;
  }

  function applyInterestV3(uint512 memory amount, uint[] memory yearlyInterestsPerMille, uint elapsedMonths) internal pure returns(uint512 memory) {
    uint iterCount = min(elapsedMonths, yearlyInterestsPerMille.length);
    uint512 memory coeff = UInt512.one();

    for (uint i = 0; i < iterCount; ++i)
      coeff.mulInPlace(uint512(0, 12000 + yearlyInterestsPerMille[i]));

    if (elapsedMonths > iterCount)
      coeff.mulInPlace((12000 + yearlyInterestsPerMille[yearlyInterestsPerMille.length - 1]).pow(elapsedMonths - iterCount));

    return amount.mul(coeff).div(uint(12000).pow(elapsedMonths));
  }
}
