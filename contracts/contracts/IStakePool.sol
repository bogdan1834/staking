// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts-upgradeable/utils/introspection/IERC165Upgradeable.sol";

interface IStakePool is IERC165Upgradeable {
  function refill(address _recipient, uint amount) external;
}
