// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts-upgradeable/token/ERC20/ERC20Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts/utils/math/SafeMath.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "@openzeppelin/contracts-upgradeable/utils/introspection/IERC165Upgradeable.sol";

contract StakingToken is
    IERC165Upgradeable,
    ERC20Upgradeable,
    OwnableUpgradeable
{
    using SafeMath for uint256;

    function __StakingToken_init(
        string memory _name,
        string memory _symbol,
        address _owner,
        uint256 _supply
    ) public initializer {
        __Ownable_init();
        __ERC20_init(_name, _symbol);
        __Staking_init_unchained(_owner, _supply);
    }

    function __Staking_init_unchained(address _owner, uint256 _supply)
        internal
        initializer
    {
        _mint(_owner, _supply);
    }

    function initialize(string memory _name, string memory _symbol, address _owner, uint256 _supply) public initializer {
      __StakingToken_init(_name, _symbol, _owner, _supply);
    }

    function supportsInterface(bytes4 interfaceId)
        external
        pure
        override
        returns (bool)
    {
        return (interfaceId == type(IERC20Upgradeable).interfaceId);
    }
}
