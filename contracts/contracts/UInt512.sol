// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

// Adapted from https://github.com/calccrypto/uint256_t/blob/master/uint128_t.cpp

struct uint512 {
  uint upper;
  uint lower;
}

library UInt512 {
  using UInt512 for uint256;
  using UInt512 for uint512;

  // -------------------- Constants
  uint constant private LOWER_HALF_MASK = 0xffffffffffffffffffffffffffffffff;

  // -------------------- Constructors
  function zero() internal pure returns(uint512 memory) {
    return uint512(0, 0);
  }

  function one() internal pure returns(uint512 memory) {
    return uint512(0, 1);
  }

  // -------------------- Conversions
  function to256Safe(uint512 memory a) internal pure returns(uint) {
    require(a.upper == 0, "uint512 too big to cast to uint256");
    return a.lower;
  }

  // -------------------- Equality and comparison
  function eq(uint512 memory a, uint512 memory b) internal pure returns(bool) {
    return a.upper == b.upper && a.lower == b.lower;
  }

  function neq(uint512 memory a, uint512 memory b) internal pure returns(bool) {
    return a.upper != b.upper || a.lower != b.lower;
  }

  function lt(uint512 memory a, uint512 memory b) internal pure returns(bool) {
    return a.upper == b.upper ? a.lower < b.lower : a.upper < b.upper;
  }

  function gt(uint512 memory a, uint512 memory b) internal pure returns(bool) {
    return a.upper == b.upper ? a.lower > b.lower : a.upper > b.upper;
  }

  function lte(uint512 memory a, uint512 memory b) internal pure returns(bool) {
    return a.upper == b.upper ? a.lower <= b.lower : a.upper <= b.upper;
  }

  function gte(uint512 memory a, uint512 memory b) internal pure returns(bool) {
    return a.upper == b.upper ? a.lower >= b.lower : a.upper >= b.upper;
  }

  function cmp(uint512 memory a, uint512 memory b) internal pure returns(uint) {
    return a.upper == b.upper ? a.lower - b.lower : a.upper - b.upper;
  }

  // -------------------- Bitwise operations
  function and(uint512 memory a, uint512 memory b) internal pure returns(uint512 memory) {
    return uint512(a.upper & b.upper, a.lower & b.lower);
  }

  function or(uint512 memory a, uint512 memory b) internal pure returns(uint512 memory) {
    return uint512(a.upper | b.upper, a.lower | b.lower);
  }

  function xor(uint512 memory a, uint512 memory b) internal pure returns(uint512 memory) {
    return uint512(a.upper ^ b.upper, a.lower ^ b.lower);
  }

  function not(uint512 memory a) internal pure returns(uint512 memory) {
    return uint512(~a.upper, ~a.lower);
  }

  function shl(uint512 memory a, uint shift) internal pure returns(uint512 memory) {
    if (shift >= 512)
      return zero();

    if (shift == 0)
      return a;

    if (shift == 256)
      return uint512(a.lower, 0);

    if (shift < 256)
      return uint512((a.upper << shift) + (a.lower >> (256 - shift)), a.lower << shift);

    // if (shift > 256)
    return uint512(a.lower << (shift - 256), 0);
  }

  function shl1InPlace(uint512 memory a) private pure {
    unchecked {
      a.upper = (a.upper << 1) + (a.lower >> 255);
      a.lower <<= 1;
    }
  }

  function shr(uint512 memory a, uint shift) internal pure returns(uint512 memory) {
    if (shift >= 512)
      return zero();

    if (shift == 0)
      return a;

    if (shift == 256)
      return uint512(0, a.upper);

    if (shift < 256)
      return uint512(a.upper >> shift, (a.upper << (256 - shift)) + (a.lower >> shift));

    // if (shift > 256)
    return uint512(0, a.upper >> (shift - 256));
  }

  function shr1InPlace(uint512 memory a) private pure {
    unchecked {
      a.lower = (a.upper << 255) + (a.lower >> 1);
      a.upper >>= 1;
    }
  }

  function highestOneBit(uint512 memory a) internal pure returns(uint) {
    uint highestBit = 0;
    uint half = 0;

    if (a.upper == 0)
      half = a.lower;
    else {
      highestBit = 256;
      half = a.upper;
    }

    while (half != 0) {
      half >>= 1;
      ++highestBit;
    }

    return highestBit;
  }

  // -------------------- Arithmetic operations
  function add(uint512 memory a, uint512 memory b) internal pure returns(uint512 memory) {
    unchecked {
      uint newLower = a.lower + b.lower;
      return uint512(a.upper + b.upper + (newLower < a.lower ? 1 : 0), newLower);
    }
  }

  function add1InPlace(uint512 memory a) private pure {
    unchecked {
      ++a.lower;

      if (a.lower == 0)
        ++a.upper;
    }
  }

  function sub(uint512 memory a, uint512 memory b) internal pure returns(uint512 memory) {
    unchecked {
      uint newLower = a.lower - b.lower;
      return uint512(a.upper - b.upper - (newLower > a.lower ? 1 : 0), newLower);
    }
  }

  function subInPlace(uint512 memory a, uint512 memory b) internal pure {
    unchecked {
      a.upper -= b.upper;

      if (a.lower < b.lower)
        --a.upper;

      a.lower -= b.lower;
    }
  }

  function mulInPlace(uint512 memory a, uint512 memory b) internal pure {
    unchecked {
      uint t = a.lower & LOWER_HALF_MASK;

      uint b1 = b.upper & LOWER_HALF_MASK;
      uint b2 = b.lower >> 128;
      uint b3 = b.lower & LOWER_HALF_MASK;

      uint p1 = t * b1;
      uint p2 = t * b2;
      uint p3 = t * b3;

      uint q4 = p3 & LOWER_HALF_MASK;
      uint q3 = (p2 & LOWER_HALF_MASK) + (p3 >> 128);
      uint q2 = (p1 & LOWER_HALF_MASK) + (p2 >> 128);
      uint q1 = ((t * (b.upper >> 128)) & LOWER_HALF_MASK) + (p1 >> 128);

      t = a.lower >> 128;
      p2 = t * b2;
      p3 = t * b3;

      q3 += (p3 & LOWER_HALF_MASK);
      q2 += (p2 & LOWER_HALF_MASK) + (p3 >> 128);
      q1 += ((t * b1) & LOWER_HALF_MASK) + (p2 >> 128);

      t = a.upper & LOWER_HALF_MASK;

      p3 = t * b3;
      p2 = (a.upper >> 128) * b3;

      q2 += (p3 & LOWER_HALF_MASK);
      q1 += ((t * b2) & LOWER_HALF_MASK) + (p3 >> 128) + (p2 & LOWER_HALF_MASK);

      q3 += q4 >> 128;
      q2 += q3 >> 128;
      q1 += q2 >> 128;

      q4 &= LOWER_HALF_MASK;
      q3 &= LOWER_HALF_MASK;
      q2 &= LOWER_HALF_MASK;
      q1 &= LOWER_HALF_MASK;

      a.upper = (q1 << 128) | q2;
      a.lower = (q3 << 128) | q4;
    }
  }

  function mul(uint512 memory a, uint512 memory b) internal pure returns(uint512 memory r) {
    r.upper = a.upper;
    r.lower = a.lower;

    r.mulInPlace(b);
  }

  function pow(uint a, uint exp) internal pure returns(uint512 memory p) {
    // Adapted from https://www.pbinfo.ro/articole/18954/exponentiere-rapida

    p.lower = 1;
    uint512 memory _a = uint512(0, a);

    for (uint k = 1; k <= exp; k <<= 1) {
      if (exp & k != 0)
        p.mulInPlace(_a);

      _a.mulInPlace(_a);
    }
  }

  function divMod(uint512 memory a, uint512 memory b) internal pure returns(uint512 memory quotient, uint512 memory remainder) {
    unchecked {
      uint512 memory _zero = zero();
      uint512 memory _one = one();

      require(neq(b, _zero), "Division by zero");

      if (b.eq(_one))
        return (a, _zero);

      if (a.eq(b))
        return (_one, _zero);

      if (a.eq(_zero) || a.lt(b))
        return (_zero, a);

      uint highestBit = a.highestOneBit();

      for (uint x = highestBit; x > 0; --x) {
        shl1InPlace(quotient);
        shl1InPlace(remainder);

        if (a.shr(x - 1).lower & 1 != 0)
          add1InPlace(remainder);

        if (remainder.gte(b)) {
          subInPlace(remainder, b);
          add1InPlace(quotient);
        }
      }
    }
  }

  function div(uint512 memory a, uint512 memory b) internal pure returns(uint512 memory) {
    (uint512 memory q,) = a.divMod(b);
    return q;
  }

  function mod(uint512 memory a, uint512 memory b) internal pure returns(uint512 memory) {
    (,uint512 memory r) = a.divMod(b);
    return r;
  }
}
