
// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "./UInt512.sol";
import "./Interest.sol";

contract InterestTestable {
  function applyInterestV1(uint512 memory amount, uint yearlyInterestPerMille, uint elapsedMonths) public pure returns(uint512 memory) {
    return Interest.applyInterestV1(amount, yearlyInterestPerMille, elapsedMonths);
  }

  function applyInterestV2(uint512 memory amount, uint yearlyInterestPerMille, uint elapsedMonths) public pure returns(uint512 memory) {
    return Interest.applyInterestV2(amount, yearlyInterestPerMille, elapsedMonths);
  }

  function applyInterestV3(uint512 memory amount, uint[] memory yearlyInterestsPerMille, uint elapsedMonths) public pure returns(uint512 memory) {
    return Interest.applyInterestV3(amount, yearlyInterestsPerMille, elapsedMonths);
  }
}
