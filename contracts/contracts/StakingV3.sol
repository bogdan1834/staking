// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/IERC20Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/introspection/IERC165Upgradeable.sol";
import "./IStakePool.sol";
import "./Interest.sol";

contract StakingV3 is Initializable, OwnableUpgradeable {
  using UInt512 for uint512;
  using Interest for uint512;

  struct Stake {
    uint amount;
    uint timestamp;
  }

  uint constant SECONDS_IN_MONTH = 2_592_000;

  IERC20Upgradeable private tokenContract;
  address private stakePool;

  uint public stakeCountLimit;
  uint public minStakeAmount;
  uint public maxStakeAmount;
  uint public minUnstakeElapsedMonths;

  mapping(address => Stake[]) public stakes;

  uint public yearlyInterestPerMille;
  uint[] public intervalInterestsPerMille;

  function __Staking_init(address _tokenContract, address _stakePool) public initializer {
    __Context_init_unchained();
    __Ownable_init_unchained();
    __Staking_init_unchained(_tokenContract, _stakePool);
  }

  function __Staking_init_unchained(address _tokenContract, address _stakePool) public initializer {
    IERC165Upgradeable tokenERC165 = IERC165Upgradeable(_tokenContract);
    require(tokenERC165.supportsInterface(type(IERC20Upgradeable).interfaceId), "Address must be an ERC20 contract.");

    IERC165Upgradeable stakePoolERC165 = IERC165Upgradeable(_stakePool);
    require(stakePoolERC165.supportsInterface(type(IStakePool).interfaceId), "Address must be a StakePool contract.");

    tokenContract = IERC20Upgradeable(_tokenContract);
    stakePool = _stakePool;

    yearlyInterestPerMille = 100;

    stakeCountLimit = 64;
    minStakeAmount = 500;
    maxStakeAmount = type(uint).max / 2;
    minUnstakeElapsedMonths = 1;
  }

  function __upgrade() public onlyOwner {
    intervalInterestsPerMille = [
      150,
      125,
      100, 100, 100,
      90, 90, 90, 90, 90, 90,
      80, 80, 80, 80, 80, 80,
      70, 70, 70, 70, 70, 70, 70,
      60, 60, 60, 60, 60, 60,
      50
    ];
  }

  function initialize(address _tokenContract, address _stakePool) public initializer() {
    __Staking_init(_tokenContract, _stakePool);
  }

  function setStakeCountLimit(uint newStakeCountLimit) public onlyOwner {
    stakeCountLimit = newStakeCountLimit;
  }

  function setMinStakeAmount(uint newMinStakeAmount) public onlyOwner {
    minStakeAmount = newMinStakeAmount;
  }

  function setMaxStakeAmount(uint newMaxStakeAmount) public onlyOwner {
    maxStakeAmount = newMaxStakeAmount;
  }

  function setMinUnstakeElapsedMonths(uint newMinUnstakeElapsedMonths) public onlyOwner {
    minUnstakeElapsedMonths = newMinUnstakeElapsedMonths;
  }

  function setYearlyInterestPerMille(uint newYearlyInterestPerMille) public onlyOwner {
    yearlyInterestPerMille = newYearlyInterestPerMille;
  }

  function setIntervalInterestsPerMille(uint[] memory newIntervalInterestsPerMille) public onlyOwner {
    intervalInterestsPerMille = newIntervalInterestsPerMille;
  }

  function getStakes(address stakeholder) public view returns (Stake[] memory) {
    return stakes[stakeholder];
  }

  function stake(uint amount) public {
    require(amount >= minStakeAmount, "Stake amount too low.");
    require(amount <= maxStakeAmount, "Stake amount too high.");
    require(stakes[msg.sender].length < stakeCountLimit, "Stake limit reached.");

    require(tokenContract.transferFrom(msg.sender, address(this), amount), "Couldn't transfer funds from stakeholder.");
    stakes[msg.sender].push(Stake(amount, block.timestamp));
  }

  function applyInterest(uint amount, uint elapsedMonths) public view returns(uint) {
    return uint512(0, amount).applyInterestV3(intervalInterestsPerMille, elapsedMonths).to256Safe();
  }

  modifier validStake(uint stakeIdx) {
    require(stakeIdx < stakes[msg.sender].length, "Invalid stake");
    _;
  }

  modifier canUnstake(uint stakeIdx) {
    require(block.timestamp - stakes[msg.sender][stakeIdx].timestamp >= minUnstakeElapsedMonths, "Too early to unstake.");
    _;
  }

  function getStakeReward(Stake storage s) private view returns(uint, uint) {
    uint elapsedMonths = (block.timestamp - s.timestamp) / SECONDS_IN_MONTH;
    uint reward = 0;

    if (elapsedMonths > 0)
      reward = applyInterest(s.amount, elapsedMonths) - s.amount;

    return (s.amount, reward);
  }

  function unstake(uint stakeIdx) public validStake(stakeIdx) canUnstake(stakeIdx) {
    Stake[] storage userStakes = stakes[msg.sender];
    Stake storage s = userStakes[stakeIdx];

    (uint amount, uint reward) = getStakeReward(s);

    require(tokenContract.balanceOf(stakePool) >= reward, "Not enough funds in stake pool.");

    require(tokenContract.transfer(msg.sender, amount), "Couldn't transfer staked tokens.");
    require(tokenContract.transferFrom(stakePool, msg.sender, reward), "Couldn't transfer stake reward.");

    userStakes[stakeIdx] = userStakes[userStakes.length - 1];
    userStakes.pop();
  }
}
