// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "./UInt512.sol";

contract UInt512Testable {
  // -------------------- Constructors
  function zero() public pure returns(uint512 memory) {
    return UInt512.zero();
  }

  function one() public pure returns(uint512 memory) {
    return UInt512.one();
  }

  // -------------------- Conversions
  function to256Safe(uint512 memory a) public pure returns(uint) {
    return UInt512.to256Safe(a);
  }

  // -------------------- Equality and comparison
  function eq(uint512 memory a, uint512 memory b) public pure returns(bool) {
    return UInt512.eq(a, b);
  }

  function neq(uint512 memory a, uint512 memory b) public pure returns(bool) {
    return UInt512.neq(a, b);
  }

  function lt(uint512 memory a, uint512 memory b) public pure returns(bool) {
    return UInt512.lt(a, b);
  }

  function gt(uint512 memory a, uint512 memory b) public pure returns(bool) {
    return UInt512.gt(a, b);
  }

  function lte(uint512 memory a, uint512 memory b) public pure returns(bool) {
    return UInt512.lte(a, b);
  }

  function gte(uint512 memory a, uint512 memory b) public pure returns(bool) {
    return UInt512.gte(a, b);
  }

  function cmp(uint512 memory a, uint512 memory b) public pure returns(uint) {
    return UInt512.cmp(a, b);
  }

  // -------------------- Bitwise operations
  function and(uint512 memory a, uint512 memory b) public pure returns(uint512 memory) {
    return UInt512.and(a, b);
  }

  function or(uint512 memory a, uint512 memory b) public pure returns(uint512 memory) {
    return UInt512.or(a, b);
  }

  function xor(uint512 memory a, uint512 memory b) public pure returns(uint512 memory) {
    return UInt512.xor(a, b);
  }

  function not(uint512 memory a) public pure returns(uint512 memory) {
    return UInt512.not(a);
  }

  function shl(uint512 memory a, uint shift) public pure returns(uint512 memory) {
    return UInt512.shl(a, shift);
  }

  function shr(uint512 memory a, uint shift) public pure returns(uint512 memory) {
    return UInt512.shr(a, shift);
  }

  function highestOneBit(uint512 memory a) public pure returns(uint) {
    return UInt512.highestOneBit(a);
  }

  // -------------------- Arithmetic operations
  function add(uint512 memory a, uint512 memory b) public pure returns(uint512 memory) {
    return UInt512.add(a, b);
  }

  function sub(uint512 memory a, uint512 memory b) public pure returns(uint512 memory) {
    return UInt512.sub(a, b);
  }

  function mul(uint512 memory a, uint512 memory b) public pure returns(uint512 memory) {
    return UInt512.mul(a, b);
  }

  function mulInPlace(uint512 memory a, uint512 memory b) public pure {
    UInt512.mulInPlace(a, b);
  }

  function pow(uint a, uint b) public pure returns(uint512 memory) {
    return UInt512.pow(a, b);
  }

  function divMod(uint512 memory a, uint512 memory b) public pure returns(uint512 memory quotient, uint512 memory remainder) {
    return UInt512.divMod(a, b);
  }

  function div(uint512 memory a, uint512 memory b) public pure returns(uint512 memory) {
    return UInt512.div(a, b);
  }

  function mod(uint512 memory a, uint512 memory b) public pure returns(uint512 memory) {
    return UInt512.mod(a, b);
  }
}
