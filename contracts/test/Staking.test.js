const StakingToken = artifacts.require("StakingToken");
const StakePool = artifacts.require("StakePool");
const StakingV1 = artifacts.require("StakingV1");
const StakingV2 = artifacts.require("StakingV2");
const StakingV3 = artifacts.require("StakingV3");
const utils = require("./helpers/utils");
const time = require("./helpers/time");
const  BigNumber =require('bignumber.js');
const expect = require('chai').expect;
const truffleAssert = require('truffle-assertions');

contract("Staking", (accounts) => {
    let stakingToken;
    let stakePool;
    let stakingContract;
    let initialSupply = 100000;
    let minimumStakeAmount = 10;
    let interestPercentage = 10;
    let stakeCountlimit = 64;
    let tokenName = 'SToken';
    let tokenSymbol = 'STKN';
    let tokenOwner = accounts[0];
    beforeEach(async () => {
        stakingToken = await StakingToken.new();
        stakePool = await StakePool.new();
        stakingContractV1 = await StakingV1.new(); 
        stakingContractV2 = await StakingV2.new(); 
        stakingContractV3 = await StakingV3.new(); 
        await stakingToken.initialize(tokenName, tokenSymbol, tokenOwner, initialSupply, {from: tokenOwner});
        await stakePool.initialize(stakingToken.address, {from: tokenOwner})
        await stakingContractV1.initialize(stakingToken.address, stakePool.address, {from: tokenOwner});
        await stakingContractV2.initialize(stakingToken.address, stakePool.address, {from: tokenOwner});
        await stakingContractV3.initialize(stakingToken.address, stakePool.address, {from: tokenOwner});
    });
    context("with staking contract", async() => {
        it("should not allow staking below minimum amount", async () => {
            await utils.shouldThrow(stakingContractV1.stake(5, {from: tokenOwner}));
        })
        it("should not allow staking above maximum amount", async () => {
            await utils.shouldThrow(stakingContractV1.stake(-1, {from: tokenOwner}));
        })
        xit("should not allow to add stake after the limit is reached", async () => {
            await stakingToken.increaseAllowance(stakingContractV1.address, 700, {from: tokenOwner});
            const supply = BigNumber(await stakingToken.allowance.call(tokenOwner, stakingContractV1.address));
            expect(supply.toString()).to.equal('700');
            for(let i = 0; i < stakeCountlimit; i++)
                await stakingContractV1.stake(minimumStakeAmount, {from: tokenOwner});
            await utils.shouldThrow(stakingContractV1.stake(minimumStakeAmount, {from: tokenOwner}));
        })
    })
    context("with V1 of interest applied", async() => {
        it("should allow staking and unstaking", async() => {
            const stakeAmount = 5000;
            let expectedResult = stakeAmount * (1200 + 10 * 24) / 1200   + stakeAmount * (1200 + 10 * 12) / 1200;
            await stakingToken.increaseAllowance(stakePool.address, 10000, {from: tokenOwner});
            await stakePool.refill(stakingContractV1.address, 10000, {from: tokenOwner});
            await stakingToken.increaseAllowance(stakingContractV1.address, stakeAmount , {from: tokenOwner});
            await stakingContractV1.stake(stakeAmount, {from: tokenOwner});
            await time.advanceTime(time.duration.days(361));
            await stakingToken.increaseAllowance(stakingContractV1.address, stakeAmount , {from: tokenOwner});
            await stakingContractV1.stake(stakeAmount, {from: tokenOwner});
            await time.advanceTime(time.duration.days(361));
            await stakingContractV1.unstake.sendTransaction(0, {from: tokenOwner});
            await stakingContractV1.unstake.sendTransaction(0, {from: tokenOwner});
            currBalance = BigNumber(await stakingToken.balanceOf(tokenOwner, {from: tokenOwner}));
            expect(currBalance.toString()).to.equal((expectedResult + initialSupply - 10000 - stakeAmount * 2).toString());
            })
    })
    context("with V2 of interest applied", async() => {
        it("should allow staking and unstaking", async() => {
            const stakeAmount = 1000;
            let expectedResult = Math.floor((stakeAmount * (1200 + 10)**24) / (1200**24))   +  Math.floor((stakeAmount * (1200 + 10)**12) / (1200**12));
            await stakingToken.increaseAllowance(stakePool.address, 10000, {from: tokenOwner});
            await stakePool.refill(stakingContractV2.address, 10000, {from: tokenOwner});
            await stakingToken.increaseAllowance(stakingContractV2.address, stakeAmount , {from: tokenOwner});
            await stakingContractV2.stake(stakeAmount, {from: tokenOwner});
            await time.advanceTime(time.duration.days(361));
            await stakingToken.increaseAllowance(stakingContractV2.address, stakeAmount , {from: tokenOwner});
            await stakingContractV2.stake(stakeAmount, {from: tokenOwner});
            await time.advanceTime(time.duration.days(361));
            await stakingContractV2.unstake.sendTransaction(0, {from: tokenOwner});
            await stakingContractV2.unstake.sendTransaction(0, {from: tokenOwner});
            currBalance = BigNumber(await stakingToken.balanceOf(tokenOwner, {from: tokenOwner}));
            expect(currBalance.toString()).to.equal((expectedResult + initialSupply - 10000 - stakeAmount * 2).toString());
        })
    })
    context("with V3 of interest applied", async() => {
        it("should allow staking and unstaking after a year", async() => {
            const intervalInterestPerMille = [
                150,
                125,
                100, 100, 100,
                90, 90, 90, 90, 90, 90,
                80, 80, 80, 80, 80, 80,
                70, 70, 70, 70, 70, 70, 70,
                60, 60, 60, 60, 60, 60,
                50
              ];
            const stakeAmount = 1000;
            let expectedResult =  stakeAmount;
            for(let i = 0; i < 12; i++)
                expectedResult *= 12000 + intervalInterestPerMille[i];
            expectedResult = Math.floor(expectedResult / (12000 ** 12));
            await stakingToken.increaseAllowance(stakePool.address, 10000, {from: tokenOwner});
            await stakePool.refill(stakingContractV3.address, 10000, {from: tokenOwner});
            await stakingToken.increaseAllowance(stakingContractV3.address, stakeAmount , {from: tokenOwner});
            await stakingContractV3.stake(stakeAmount, {from: tokenOwner});
            await time.advanceTime(time.duration.days(361));
            await stakingContractV3.unstake.sendTransaction(0, {from: tokenOwner});
            currBalance = BigNumber(await stakingToken.balanceOf(tokenOwner, {from: tokenOwner}));
            expect(currBalance.toString()).to.equal((expectedResult + initialSupply - 10000 - stakeAmount).toString());
        })
    })


})