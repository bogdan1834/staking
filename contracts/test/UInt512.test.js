const UInt512Testable = artifacts.require("UInt512Testable")
const { assert } = require("chai")
const BN = require("bignumber.js")

const expect = (actual, expected) =>
  assert.equal(actual.upper.toString(), expected.upper.toString())
  && assert.equal(actual.lower.toString(), expected.lower.toString())

contract("UInt512", async accounts => {
  let u

  before(async () => {
    u = await UInt512Testable.new()
  })

  it("should add correctly", async () => {
    const _5 = await u.add({ upper: 0, lower: 2 }, { upper: 0, lower: 3 })
    expect(_5, { upper: 0, lower: 5 })

    const uint256Half = new BN(2 ** 255).toFixed()

    const upper1 = await u.add({ upper: 0, lower: uint256Half }, { upper: 0, lower: uint256Half })
    expect(upper1, { upper: 1, lower: 0 })
  })

  it("should subtract correctly", async () => {
    const _2 = await u.sub({ upper: 0, lower: 6 }, { upper: 0, lower: 4 })
    expect(_2, { upper: 0, lower: 2 })

    const uint256Max = new BN(2 ** 256 - 1).toFixed()

    const expectMax = await u.sub({ upper: 5, lower: 0 }, { upper: 4, lower: 1 })
    expect(expectMax, { upper: 0, lower: uint256Max })
  })

  it("should multiply correctly", async () => {
    const _24 = await u.mul({ upper: 0, lower: 8 }, { upper: 0, lower: 3 })
    expect(_24, { upper: 0, lower: 24 })

    const uint256Half = new BN(2 ** 255).toFixed()

    const upper1 = await u.mul({ upper: 0, lower: 2 }, { upper: 0, lower: uint256Half })
    expect(upper1, { upper: 1, lower: 0 })
  })

  it("should divide correctly", async () => {
    const r = await u.divMod({ upper: 0, lower: 11 }, { upper: 0, lower: 4 })

    expect(r.quotient, { upper: 0, lower: 2 })
    expect(r.remainder, { upper: 0, lower: 3 })

    const r2 = await u.divMod({ upper: 1, lower: 20 }, { upper: 0, lower: 8 })
    const expected = new BN(2 ** 253 + 2).toFixed()

    expect(r2.quotient, { upper: 0, lower: expected })
    expect(r2.remainder, { upper: 0, lower: 4 })
  })

  it("should exponentiate correctly", async () => {
    const p = await u.pow(5, 0)
    expect(p, { upper: 0, lower: 1 })

    const p2 = await u.pow(2, 257)
    expect(p2, { upper: 2, lower: 0 })
  })
})
