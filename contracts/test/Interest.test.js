const InterestTestable = artifacts.require("InterestTestable")
const { assert } = require("chai")
const BN = require("bignumber.js")

const expect = (actual, expected) =>
  assert.equal(actual.upper.toString(), expected.upper.toString())
  && assert.equal(actual.lower.toString(), expected.lower.toString())

contract("Interest", async accounts => {
  let i

  before(async () => {
    i = await InterestTestable.new()
  })

  it("should compute V1 correctly", async () => {
    const r = await i.applyInterestV1({ upper: 0, lower: 5000 }, 150, 12)
    expect(r, { upper: 0, lower: 5750 })

    const r2 = await i.applyInterestV1({ upper: 0, lower: 1000000 }, 200, 24)
    expect(r2, { upper: 0, lower: 1400000 })
  })

  it("should compute V2 correctly", async () => {
    const r = await i.applyInterestV2({ upper: 0, lower: 5000 }, 150, 12)
    expect(r, { upper: 0, lower: 5803 })

    const r2 = await i.applyInterestV2({ upper: 0, lower: 1000000 }, 100, 24)
    expect(r2, { upper: 0, lower: 1220390 })
  })

  it("should compute V3 correctly", async () => {
    const r = await i.applyInterestV3({ upper: 0, lower: 5000 }, [150], 12)
    expect(r, { upper: 0, lower: 5803 })

    // https://docs.google.com/spreadsheets/d/1AcFrsNzKfQS6-m7MgFCkcYZVvaRs8TAMVPa9-fJY_FM
    const intervals = [150, 125, 100, 100, 100, 90, 90, 90, 90, 90, 90, 80, 80, 80, 80, 80, 80, 70, 70, 70, 70, 70, 70, 70, 60, 60, 60, 60, 60, 60]
    const r2 = await i.applyInterestV3({ upper: 0, lower: 10000 }, intervals, intervals.length - 1)
    expect(r2, { upper: 0, lower: 12190 })
  })
})
