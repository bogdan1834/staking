const StakingToken = artifacts.require("StakingToken");
const utils = require("./helpers/utils");
const BigNumber = require('bignumber.js');
var expect = require('chai').expect;

contract("StakingToken", (accounts) => {
    let contractInstance;
    let initialSupply = 10;
    let tokenName = 'SToken';
    let tokenSymbol = 'STKN';
    let tokenOwner = accounts[0];
    beforeEach(async () => {
        contractInstance = await StakingToken.new();
    });
    xit("should be able to create a token with a fixed supply", async () => {
        const result = await contractInstance.initialize(tokenName, tokenSymbol, tokenOwner, initialSupply);
        expect(result.receipt.status).to.equal(true);
        const name = await contractInstance.name();
        expect(name).to.equal(tokenName);
        const symbol = await contractInstance.symbol();
        expect(symbol).to.equal(tokenSymbol);
        const supply = BigNumber(await contractInstance.totalSupply.call());
        expect(supply.toString()).to.equal(initialSupply.toString());
    })
})