const { deployProxy, upgradeProxy } = require("@openzeppelin/truffle-upgrades")

const StakingV2 = artifacts.require("StakingV2")
const StakingV3 = artifacts.require("StakingV3")


module.exports = async (deployer, network, accounts) => {
  const stakingV2 = await StakingV2.deployed()
  const stakingV3 = await upgradeProxy(stakingV2.address, StakingV3)
  await stakingV3.__upgrade()
}
