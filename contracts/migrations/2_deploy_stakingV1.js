// See https://docs.openzeppelin.com/upgrades-plugins/1.x/

const { deployProxy } = require("@openzeppelin/truffle-upgrades")
const { writeFileSync } = require("fs")

const StakingToken = artifacts.require("StakingToken")
const StakePool = artifacts.require("StakePool")
const StakingV1 = artifacts.require("StakingV1")


module.exports = async (deployer, network, accounts) => {
  const stakingToken = await deployProxy(StakingToken, ["StakingToken", "TT", accounts[0], BigInt(10000000)], { deployer })
  const stakePool = await deployProxy(StakePool, [stakingToken.address], { deployer })
  const stakingV1 = await deployProxy(StakingV1, [stakingToken.address, stakePool.address], { deployer })

  const addresses = {
    StakingToken: stakingToken.address,
    StakePool: stakePool.address,
    StakingV1: stakingV1.address
  }

  writeFileSync(`migrations/addresses-${network}.json`, JSON.stringify(addresses))
}
