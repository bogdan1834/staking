const { deployProxy, upgradeProxy } = require("@openzeppelin/truffle-upgrades")

const StakingV1 = artifacts.require("StakingV1")
const StakingV2 = artifacts.require("StakingV2")


module.exports = async (deployer, network, accounts) => {
  const stakingV1 = await StakingV1.deployed()
  const stakingV2 = await upgradeProxy(stakingV1.address, StakingV2)
}
