#!/usr/bin/env bash
set -e

pid=`lsof -Pn -i4 | grep 8545 | sed -r "s/ +/ /g" | cut -f 2 -d " "`

if [ -n "$pid" ]; then
  echo "Killing old Ganache instance..."
  kill $pid
fi

testnet=kovan
ganache-cli -f "https://$testnet.infura.io/v3/$INFURA_API_KEY" -m "$METAMASK_PHRASE" --networkId 1 --chainId 1337
