#!/usr/bin/env bash
set -euo pipefail

ABI_DIR="build/contracts"
FRONTEND_DIR="../staking-ui/src/utils"

mkdir -p "$FRONTEND_DIR/addresses"
cp "migrations/addresses-rinkeby.json" "$FRONTEND_DIR/addresses/addresses-rinkeby.json"

eth-binding-gen \
  --backend typescript \
  --abi-dir "$ABI_DIR" \
  --out-dir "$FRONTEND_DIR/contracts" \
  --overwrite
