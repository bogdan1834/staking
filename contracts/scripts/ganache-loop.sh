#!/usr/bin/env bash
set -e

frequency=5m

while true; do
  notify-send "Restarting Ganache (Frequency: $frequency)" # Sends GNOME Notification
  ./scripts/ganache.sh &
  sleep $frequency
done
