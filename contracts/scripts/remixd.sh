#!/usr/bin/env bash
set -e

if [ "$#" -eq 0 ]; then
  echo "Need filepath."
  exit 1
fi

remixd -s "$1" --remix-ide https://remix.ethereum.org
